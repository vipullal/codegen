# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Version 1.0.0

This program will generate iOs (Objective-C) and Android (Java) code for persisting objects into an SQLite database.

It is written in C++ and will compile on any platform (Unix, Mac or Windows), though you will require cmake to generate the makefiles for your platform using cmake. Note: You can also use cmake to generate an IDE for your platform - please refer to the cmake documentation for your platform.

I have tested it on Mac and cygwin under Windows.

The code requires C++ 14 compatible compiler.


### How do I get set up? ###

Note: It is best to keep the source folders clean and devoid of makefiles and compiled code. Therefore, I would recommend that you create a separate build directory and keep the source directory under src clean.


1. Checking out the code. For example, if you keep your projects in ~/projects/open_src/, a sample session would be:

	~/> mkdir -p projects/open_source/codegen
	~/> cd projects/open_source/codegen
	~/projects/open_source/codegen >git clone https://vipullal@bitbucket.org/vipullal/codegen.git .

	Remember to put a "." at the end of the command as otherwise git will clone into a sub-folder!

2. Create a build directory at the level where you checked out the code. For example, if you used the above commands to checkout:

	~/projects/open_source/codegen >mkdir build
	cd build 
        
3. Run cmake to generate your platform specific makefiles:

	~/projects/open_source/codegen/build >cmake ../src

	This will create the required make files. 
	_Note_: You can use cmake to make an IDE for your platform and debug the code also. Please refer to the cmake documentation for your platform.
	_Note_: We have packaged some makefiles for mac, linux and cygwin. You can find these under makefiles/mac, makefiles/linux and makefiles/cygwin and these should work off the bat. Please let us know of any problems!

4. Run make:

		~/projects/open_source/codegen/build >make

	This will compile the code and create the executable. If you built the debug target, the executable will be in:

 		./build/codegen/Debug/codegen

5. We have packaged a sample, test.xml project under:

	<checkout-dir>/src/sample1/test.xml

6. To run the program from the command line, open a shell session and cd to where the compiled binary. The program takes the following arguments:

	`<path to xml file> [platforms] -d`

	where:
	`<path to xml file>` is the complete or relative path to the xml file
	`[platforms]` can be Android, ios Swift (this is WIP) or all. All ignore case, so you could give it Android or ANDROID. Default is ALL
	`-d` This flag is the debug flag. It generates the content to screen and does not write any files


7. The xml file structure:
   
	The xml file basically consists of two sets of tags:

	1.  "config" tags. These specify platform specific 
	2.  "dbobject and dbcolumn" tags which specify the database objects.

	Android config tag can contain a namespace and a genpath tag. For example:
	`
	<android>
		<namespace>com.example.vipullal.dbtestapp</namespace>
		<genpath>./android</genpath>
	</android>
	`

	The Objective-C config tag can contain the genpath tag. For example:

	`
	<objc>
		<genpath>./ios</genpath>
	<objc>
	`

	The `<dbobject>` tag defines an object which will be persisted to the database. 

	A DBObject consists of one or more DBColumn elements. Each DBColumn element describes the column name and data type. For each DBObject, codeine will generate a separate class and properties (along with getter and setters). It will also generate a DatabaseAdapter class which has methods to create, update, delete DBObjects.  
	The DBColumn types can be:

		1. Identity. This is the standard auto-increment __ROWID__ in SQLite. The code will automatically populate this when you add a new object
		2. String: Normal text 
		3. Date: There are 2 date types; date (eg dd-mm-yyyy) or date-time. Since SQLite does not have a date datatype, the generated code takes care of converting to/from native date type to text. 
		4. Bool: Boolean values, stored as Y or N in the database. Generated code does the conversion.
		5. Numeric: Standard double.
		6. ForeignKey: Standard long value, can be used to reference the primary-key of another table.

### Contribution guidelines ###

If you want some specific enhancements or changes, please feel free to reach out to me at:
	support@fareastsoftware.com
or
	via bitbucket 

### Who do I talk to? ###

* Repo owner or admin
