/*
 File: OBJCGenerator.cpp
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#include "OBJCGenerator.h"
#include "DBObject.h"
#include "TargetTypes.h"

#include <iostream>
#include <fstream>
#include <functional>
#include <sstream>
#include <regex>


namespace
{
    const char *FieldTypeToLocalType( DBCol::eColumnTypes inType)
    {
        switch(inType)
        {
            case DBCol::eColType_int:
                return "%d";
            case DBCol::eColType_number:
                return "%f";
            case DBCol::eColType_bool:
                return "%d";
            case DBCol::eColType_text:
                return "'%@'";
            case DBCol::eColType_date:
                return "'%@'";     // yyyymmdd
            case DBCol::eColType_datetime:
                return "'%@'";
            case DBCol::eColType_identity:
            case DBCol::eColType_foreignkey:
                return "%ld";
            default:
                return "--Invalid type--";
        }
    };
    
    
    const char *DBTypeToDataType( DBCol::eColumnTypes inType)
    {
        switch(inType)
        {
            case DBCol::eColType_int:
                return "int";
            case DBCol::eColType_number:
                return "double";
            case DBCol::eColType_bool:
                return "BOOL";
            case DBCol::eColType_text:
                return "NSString*";
            case DBCol::eColType_date:
                return "NSDate*";     // todo: yyyymmdd
            case DBCol::eColType_datetime:
                return "'%s'";
            case DBCol::eColType_foreignkey:
            case DBCol::eColType_identity:
                return "long";
            default:
                return "--Invalid type--";
        }
    };
};



// ----------------------------------------------------------------
//      generateDatabaseAdapterHeaderFile
//  ----------------------------------------------------------------
std::string OBJCGenerator::generateDatabaseAdapterHeaderFile()
{
    std::ostringstream  opStream;
    opStream << "\n//\n// DatabaseAdapte.h\n//" << this->m_Dictonary.getCopyrightString();
    opStream << "\n\n#ifndef DatabaseAdapter_h\n#define DatabaseAdapter_h\n\n";
    opStream << "\n\n#include <sqlite3.h>";
    // Add include files for each object
    this->m_Dictonary.doEachObject( [&]( const std::unique_ptr<DBObject> & o ) { opStream << "\n#include \"" << o->getName() << ".h\"";} );
    opStream << "\n\n\n// *********************************************" \
    "\n@interface DatabaseAdapter : NSObject"\
    "\n{"\
    "\n\tsqlite3* m_DB;"\
    "\n\tNSDateFormatter* _DateOnlyFormatter;"\
    "\n\tNSDateFormatter* _DateTimeFormatter;"\
    "\n\tBOOL _logging;"
    "\n}"\
    "\n\n"\
    "\n// Date and DateTime..."\
    "\n-(NSString* )formatDateForDB:(NSDate *) inDate;"\
    "\n-(NSDate* )getDateFromDB:(const unsigned char *)inStr;"\
    "\n-(NSString* )formatDateTimeForDB:(NSDate *) inDate;"\
    "\n-(NSDate* )getDateTimeFromDB:(const unsigned char *)inStr;"\
    "\n-(void) setLogging:(BOOL)onOff;"\
    "\n\n"\
    "\n+(NSString*) getNullableString:(sqlite3_stmt *) stmt atIndex:(int)inIndex;"\
    "\n+(NSDate*) getNullableDate:(sqlite3_stmt *) stmt atIndex:(int)inIndex;"\
    "\n+(NSDate*) getNullableDateTime:(sqlite3_stmt *) stmt atIndex:(int)inIndex;";

    this->m_Dictonary.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                                   {
                                       std::string name = o->getName();
                                       opStream << "\n\n// CRUID for " << name;
                                       opStream << "\n-(BOOL) add" << name << ":(" << name <<"*) o;";
                                       opStream << "\n-(BOOL) update"<< name << ":(" << name <<"*) o;";
                                       opStream << "\n-(BOOL) delete" << name << "WithId:(long) inId;";
                                       opStream << "\n-(BOOL) deleteAll" << name << ";";
                                       opStream << "\n-(" << name << "*) get" << name << "WithId: (long)inId;";
                                       opStream << "\n-(NSArray*) getAll" << name << ";";
                                   } );
    
    opStream << "\n\n\n-(BOOL)openForBusiness:(NSString*)filePath;"\
    "\n+(DatabaseAdapter *)getInstance;"
    "\n\n@end"\
    "\n#endif // DatabaseAdapter_h\n";
    
    return opStream.str();
}



// ----------------------------------------------------------------
void generateDBAdapterFunctionsForObject( std::ostream & os, const std::unique_ptr<DBObject>& o)
{
    std::string name = o->getName();
    os << "\n\n// CRUID for " << name ;
    // Add
    os << "\n-(BOOL) add" << name << ":(" << name <<"*) o;" \
        "\n{"\
        "\n\tint result = 0;"\
        "\n\tNSString* sql = [o getInsertSQL];"\
        "\n\tif(_logging) "\
        "\n\t\tNSLog(@\"\\nInsert SQL is %@\", sql);"\
        "\n\tchar *err;"\
        "\n\tresult = sqlite3_exec(m_DB, [sql UTF8String],NULL, NULL, &err);"\
        "\n\tif( result != SQLITE_OK && err )"\
        "\n\t{"\
        "\n\t\tNSLog(@\"Error returned by insert is  %s\", err );"\
        "\n\t}"\
        "\n\telse"\
        "\n\t{"\
        "\n\t\t[o setDatabaseId:sqlite3_last_insert_rowid(m_DB)];"\
        "\n\t}"\
        "\n\treturn result == SQLITE_OK;"\
    "\n}";
    
    // deleteWithId
    os << "\n\n// ------------------------------------------"\
    "\n-(BOOL) delete" << name << "WithId:(long) inId"\
        "\n{"\
        "\n\tBOOL success = false;"\
        "\n\treturn success;"\
        "\n}";

    // deleteAll
    os << "\n\n// ------------------------------------------"\
    "\n-(BOOL) deleteAll" << name << "\n{"\
        "\n\tint result = 0;"\
        "\n\tconst char* sql = [Person getDeleteAllSQL];"\
        "\n\tif(_logging )"\
        "\n\t\tNSLog(@\"\\nExecuting %s\",sql);"\
        "\n\tchar *err;"\
        "\n\tresult = sqlite3_exec(m_DB, sql,NULL, NULL, &err);"\
        "\n\tif( result != SQLITE_OK && err )"\
        "\n\t{"\
        "\n\t\tNSLog(@\"Error returned by deleteAll %s\", err );"\
        "\n\t}"\
        "\n\treturn result == SQLITE_OK;"
        "\n}";
    
    // update
    os << "\n\n// ---------------------------------------"
        "\n-(BOOL) update" << name << ":(" << name <<"*) o;"\
        "\n{"\
        "\n\tNSString* updateSQL = [o getUpdateSQL];"\
        "\n\tchar *err;"\
        "\n\tif( _logging )"\
        "\n\t\tNSLog(@\"\\nExecuting %@\",updateSQL);"\
        "\n\tint success =sqlite3_exec(m_DB, [updateSQL UTF8String],NULL, NULL, &err);"\
        "\n\tif( success != SQLITE_OK && err )"\
        "\n\t{"\
        "\n\t\tNSLog(@\"\\nError returned by deleteAll %s\", err );"\
        "\n\t}"\
        "\n\treturn success == SQLITE_OK;"\
       "\n}\n";
    
    // getWithId
    os << "\n\n// ---------------------------------------"
        "\n-(" << name << "*) get" << name << "WithId:(long) inId"\
        "\n{"\
        "\n\t" << name << "* o = NULL;"\
        "\n\tconst char *sql = [" << name << " getSelectByIdSQL:inId];"\
        "\n\tif(_logging)"\
        "\n\t\tNSLog(@\"Executing:%s\",sql);"\
        "\n\tint result = 0;"
        "\n\tsqlite3_stmt* aStmt = NULL;"\
        "\n\t@try\n\t{"
        "\n\t\tresult = sqlite3_prepare_v2( m_DB, sql,-1, &aStmt, NULL );"\
        "\n\t\tif( result == SQLITE_OK) "\
        "\n\t\tresult = sqlite3_step( aStmt);"\
        "\n\t\t\tif( result == SQLITE_ROW)"\
        "\n\t\t\t\to=[[ " << name << " alloc] initFromStatement:aStmt];"\
        "\n\t}"\
        "\n\t@finally\n\t{"\
        "\n\t\tif(aStmt)"\
        "\n\t\t\tsqlite3_finalize(aStmt);"\
        "\n\t\t}"
        "\n\treturn o;"\
        "\n}";

    // getAll
    os << "\n\n// ---------------------------------------"\
        "\n-(NSMutableArray *) getAll" << name << ""\
        "\n{"\
        "\n\tNSMutableArray* records = [[NSMutableArray alloc] init];"\
        "\n\tint result = 0;"\
        "\n\tconst char* sql = [Person getSelectAllSQL];"\
        "\n\tsqlite3_stmt* aStmt = NULL;"\
        "\n\t@try"\
        "\n\t{"\
        "\n\t\tresult = sqlite3_prepare_v2( m_DB, sql,-1, &aStmt, NULL );"\
        "\n\t\tif( result != SQLITE_OK)"\
        "\n\t\t{"\
        "\n\t\t\tNSLog(@\"Error while retrieving records\");"\
        "\n\t\t\treturn records;"\
        "\n\t\t}"\
        "\n\t\tdo"\
        "\n\t\t{"\
        "\n\t\t\tresult = sqlite3_step( aStmt);"\
        "\n\t\t\tif( result == SQLITE_ROW)"\
        "\n\t\t\t{"\
        "\n\t\t\t\t" << name << " *p = [[ " << name << " alloc] initFromStatement:aStmt];"\
        "\n\t\t\t\t[records addObject:p];"\
        "\n\t\t\t}"\
        "\n\t\t}"\
        "\n\t\twhile( result == SQLITE_ROW);"\
        "\n\t}"\
        "\n\t@finally"\
        "\n\t{"\
        "\n\t\tif(aStmt )"\
        "\n\t\t\tsqlite3_finalize(aStmt);"\
        "\n\t}"\
        "\n\treturn records;"\
        "\n}";
}


// ----------------------------------------------------------------
//      generateDatabaseAdapterHeaderFile
//  ----------------------------------------------------------------
std::string OBJCGenerator::generateDatabaseAdapterModule()
{
    std::ostringstream  opStream;
    opStream << "\n//\n// DatabaseAdapte.m\n//" << this->m_Dictonary.getCopyrightString();
    opStream << "\n#import <Foundation/Foundation.h>"\
        "\n#include \"DatabaseAdapter.h\""\
        "\n"\
        "\n@implementation DatabaseAdapter"\
        "\n"\
        "\nstatic DatabaseAdapter* s_Instance;"\
        "\n"\
        "\n// -------------------------------------------"\
        "\n-(instancetype) init"\
        "\n{"\
        "\n\tself = [super init];"\
        "\n\tif (self) {"\
        "\n\t\t_DateOnlyFormatter = [[NSDateFormatter alloc]init];"\
        "\n\t\t[_DateOnlyFormatter setDateFormat:@\"yyyyMMdd\"];"\
        "\n\t\t_DateTimeFormatter = [[NSDateFormatter alloc]init];"\
        "\n\t\t[_DateTimeFormatter setDateFormat:@\"yyyy-MM-dd HH:mm:ss ZZZ\"];    // TODO:"\
        "\n\t\t_logging = true;    // by default, log all statements excuted."\
        "\n\t}"\
        "\n\treturn self;"\
        "\n}"\
        "\n"\
        "\n// Date and DateTime..."\
        "\n// -------------------------------------------"\
        "\n-(NSString* )formatDateForDB:(NSDate *) inDate"\
        "\n{"\
        "\n\treturn [_DateOnlyFormatter stringFromDate: inDate];"\
        "\n}"\
        "\n"\
        "\n// -------------------------------------------"\
        "\n-(NSDate* )getDateFromDB:(const unsigned char *)inStr"\
        "\n{"\
        "\n\tNSString* s =[[NSString alloc] initWithCString:(const char*)inStr encoding: SQLITE_UTF8];"\
        "\n\treturn [_DateOnlyFormatter dateFromString: s];"\
        "\n}"\
        "\n// -------------------------------------------"\
        "\n-(NSString* )formatDateTimeForDB:(NSDate *) inDate"\
        "\n{"\
        "\n\treturn [_DateTimeFormatter stringFromDate: inDate];"\
        "\n}"\
        "\n// -------------------------------------------"\
        "\n-(NSDate* )getDateTimeFromDB:(const unsigned char *)inStr"\
        "\n{"\
        "\n\tNSString* s =[[NSString alloc] initWithCString:(const char*)inStr encoding: SQLITE_UTF8];"\
        "\n\treturn [_DateTimeFormatter dateFromString: s];"\
        "\n}\n"\
        "\n// -------------------------------------------"\
        "\n-(void) setLogging:(BOOL) onOff"\
        "\n{\n\t_logging = onOff;\n}"
        "\n"\
        "\n"\
    
        "\n// -------------------------------------------"\
        "\n+(NSString*) getNullableString:(sqlite3_stmt *) stmt atIndex:(int)inIndex"\
        "\n{"\
        "\n\tNSString * result = nil;"\
        "\n\tconst unsigned char * colData = sqlite3_column_text(stmt, inIndex);"\
        "\n\tif( colData ){"\
        "\n\t\tresult =[NSString stringWithUTF8String: (const char *)colData];"\
        "\n\t}"\
        "\n\treturn result;"\
        "\n}"\
        "\n"\
        "\n// -------------------------------------------"\
        "\n+(NSDate*) getNullableDate:(sqlite3_stmt *) stmt atIndex:(int)inIndex"\
        "\n{"\
        "\n\tNSDate* result = nil;"\
        "\n\tconst unsigned char * colData = sqlite3_column_text(stmt, inIndex);"\
        "\n\tif( colData ){"\
        "\n\t\tresult = [[DatabaseAdapter getInstance] getDateFromDB:colData];"\
        "\n\t}"\
        "\n\treturn result;"\
        "\n}"\
        "\n"\
        "\n// -------------------------------------------"\
        "\n+(NSDate*) getNullableDateTime:(sqlite3_stmt *) stmt atIndex:(int)inIndex"\
        "\n{"\
        "\n\tNSDate* result = nil;"\
        "\n\tconst unsigned char * colData = sqlite3_column_text(stmt, inIndex);"\
        "\n\tif( colData ){"\
        "\n\t\tresult = [[DatabaseAdapter getInstance] getDateTimeFromDB:colData];"\
        "\n\t}"\
        "\n\treturn result;"\
        "\n}\n\n"\
    
        "\n// -------------------------------------------"\
        "\n+(DatabaseAdapter *)getInstance"\
        "\n{"\
        "\n\tif( !s_Instance)"\
        "\n\t\ts_Instance = [[DatabaseAdapter alloc]init];"\
        "\n\treturn s_Instance;"\
        "\n}"\
        "\n// -------------------------------------------"\
        "\n-(BOOL)openForBusiness:(NSString*)inPath"\
        "\n{"\
        "\n\tBOOL success = false;"\
        "\n\tif(!m_DB){"\
        "\n\t\tNSLog(@\"Path to database: %@\",inPath);"\
        "\n\t\tBOOL openDatabaseResult = sqlite3_open([inPath UTF8String], &m_DB);"\
        "\n\t\tif( openDatabaseResult == SQLITE_OK ){"\
        "\n\t\t\tNSLog(@\"Opened the database!\");"\
        "\n\t\t\t// Make sure all tables exist..."
        "\n\t\t\tchar *err;"\
        "\n\t\t\tint result;";
    // Run an CREATE TABLE IF NOT EXISTS for each object
    this->m_Dictonary.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
           {
               opStream << "\n\t\t\tresult = sqlite3_exec(m_DB, [" << o->getName() << " getCreateTableSQL], NULL, NULL, &err);";
               opStream << "\n\t\t\tif( result != SQLITE_OK) return success;";
           } );
     opStream <<    "\n\t\t\tsuccess=TRUE;"\
        "\n\t\t}"\
        "\n\t}"\
        "\n\telse{"\
        "\n\t\tNSLog(@\"Error: Calling openForBusiness on an already opened database? Ignored..\");"\
        "\n\t}"\
        "\n\treturn success;"\
    "\n}";
    
    // Now the CRUID functions for each DBObjct.
    // Run an CREATE TABLE IF NOT EXISTS for each object
    this->m_Dictonary.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                                   {
                                       generateDBAdapterFunctionsForObject( opStream, o);
                                   } );

    opStream << "\n@end\n";
    return opStream.str();
}

// **************************************
//      HeaderFileGenerator: Generate a .h file for each DBObject
// **************************************
struct HeaderFileGenerator
{
    std::ostringstream  opStream;
    std::string m_CopyrightString;
    HeaderFileGenerator(std::string inCopyrightString )
    :m_CopyrightString(inCopyrightString)
    {
        
    }
    void generateForObject( const std::unique_ptr<DBObject>& o);
    void writeToFile( std::string path);
    std::string toStr()
    {
        return opStream.str();
    }
};


// ------------------------------------------------------
void HeaderFileGenerator::generateForObject( const std::unique_ptr<DBObject>& o)
{
    opStream.str("");   // clear old stuff
    opStream << "\n// " << o->getName() << ".h\n//" << m_CopyrightString;
    std::string prelude("\n//"\
                        "\n"\
                        "\n#ifndef @1_h"\
                        "\n#define @1_h"\
                        "\n\n#include <sqlite3.h>"\
                        "\n"\
                        "\n//******************************************"\
                        "\n//      interface @1"\
                        "\n//******************************************"\
                        "\n"\
                        "\n@interface @1 : NSObject"\
                        "\n{"\
                        "\n\t// "\
                        "\n}"\
                        "\n");
    
    std::regex class_name_regx("@1");
    opStream << std::regex_replace(prelude,class_name_regx, o->getName());
    // Generate member variables...
//    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
//                    {
//                        if( col->getColumnType() == DBCol::eColType_identity)
//                            opStream << "\n\t" << DBTypeToDataType(col->getColumnType()) << "\t\t databaseId;";
//                        else
//                            opStream << "\n\t" << DBTypeToDataType(col->getColumnType()) << "\t\t" << col->getColumnName() <<";";
//                    });
    // Generate properties...
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              opStream << "\n@property " << DBTypeToDataType(col->getColumnType()) << "\t\tdatabaseId;";
                          else
                              opStream << "\n@property " << DBTypeToDataType(col->getColumnType()) << "\t\t" << col->getColumnName() <<";";
                      });
    opStream << "\n\n-(NSString *)description;"\
        "\n-(instancetype)init;        // Defaut constructor"\
        "\n-(instancetype)initFromStatement:(sqlite3_stmt *)aStmt;     // Create from the database"\
        "\n-(NSString*) getInsertSQL;"\
        "\n-(NSString*)getUpdateSQL;"\
        "\n"\
        "\n// Static methods."\
        "\n+(const char * )getCreateTableSQL;"\
        "\n+(const char *) getSelectByIdSQL: (long)inId;"\
        "\n+(const char *) getSelectAllSQL;"\
        "\n+(const char *) getDeleteByIdSQL:(long)inId;"\
        "\n+(const char *) getDeleteAllSQL;"\
    
        "\n"\
        "\n@end"\
        "\n#endif //";
    opStream << o->getName() << ".h\n";
}


// **************************************
//      HeaderFileGenerator: Generate a .m file for each DBObject
// **************************************
struct ModuleFileGenerator
{
    std::ostringstream  opStream;
    std::string m_CopyrightString;
    ModuleFileGenerator(std::string inCopyrightString )
    :m_CopyrightString(inCopyrightString)
    {
    }
    void generateForObject( const std::unique_ptr<DBObject>& o);
    void writeToFile( std::string path);
    std::string toStr()
    {
        return opStream.str();
    }
private:
    void generateCreateTableStuff( const std::unique_ptr<DBObject>& o);
    void getSelectByIdSQL( const std::unique_ptr<DBObject>& o);
    void getDeleteSQL( const std::unique_ptr<DBObject>& o);
    void generateConstructors( const std::unique_ptr<DBObject>& o);
    void generateInsertSQL(const std::unique_ptr<DBObject>& o);
    void generateUpdateSQL( const std::unique_ptr<DBObject>& o);
};


// ------------------------------------------------------
void ModuleFileGenerator::generateCreateTableStuff( const std::unique_ptr<DBObject>& o)
{
    // Create table SQL
    opStream << "\n\n// ------------------------------------------"\
        "\n+(const char *)getCreateTableSQL"\
        "\n{"
        << "\n\treturn "
        << o->getCreateTableSQL(3,'\\')
        << "\n}\n";
}


// ------------------------------------------------------
void ModuleFileGenerator::getSelectByIdSQL( const std::unique_ptr<DBObject>& o)
{
    // getSelectByIdSQL
    opStream << "\n// ----------------------------------------"\
    "\n+(const char *)getSelectByIdSQL: (long)inId"\
    "\n{"\
    "\n\tNSString* str = [NSString stringWithFormat:@\"SELECT * from " ;
    opStream << o->getName() << " where id= %ld\", inId];"\
    "\n\treturn [str UTF8String];"\
    "\n}"\
    "\n";
}

// ------------------------------------------------------
void ModuleFileGenerator::getDeleteSQL( const std::unique_ptr<DBObject>& o)
{
    // getDeleteByIdSQL
    opStream << "\n// ----------------------------------------"\
    "\n+(const char *)getDeleteByIdSQL: (long)inId"\
    "\n{"\
    "\n\tNSString* str = [NSString stringWithFormat:@\"DELETE from ";
    opStream << o->getName() << " where id= %ld\", inId];"\
    "\n\treturn [str UTF8String];"\
    "\n}"\
    "\n"\
    "\n// ----------------------------------------"\
    "\n+(const char *)getDeleteAllSQL"\
    "\n{"\
    "\n\treturn \"DELETE from ";
    opStream << o->getName() << "\";"\
    "\n}";
}


// ------------------------------------------------------
void ModuleFileGenerator::generateConstructors( const std::unique_ptr<DBObject>& o)
{
    // default constructor for object
    opStream << "\n\n\n// ----------------------------------------"\
    "\n-(instancetype)init"\
    "\n{"\
    "\n\tself = [super init];"\
    "\n\tif (self) {"\
    "\n\t\tself.databaseId = -1;"\
    "\n\t\t// TODO: Initialize other member variables as per your business logic here.."\
    "\n\t}"\
    "\n\treturn self;"\
    "\n}";
    
    // Init from statement
    opStream << "\n// ----------------------------------------"\
    "\n-(instancetype)initFromStatement:(sqlite3_stmt *)aStmt"\
    "\n{"\
    "\n\tself = [super init];"\
    "\n\tif (self) {"\
    "\n\t\tint anIndex = 0;";
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          switch( col->getColumnType())
                          {
                              case DBCol::eColType_int:
                                  opStream << "\n\t\tself." << col->getColumnName() << " = sqlite3_column_int( aStmt, anIndex++);" ;
                                  break;
                              case DBCol::eColType_number:
                                  opStream << "\n\t\tself." << col->getColumnName() << " = sqlite3_column_double( aStmt, anIndex++);" ;
                                  break;
                              case DBCol::eColType_bool:
                                  opStream << "\n\t\tself." << col->getColumnName() << " = sqlite3_column_int( aStmt, anIndex++)? TRUE:FALSE;" ;
                                  break;
                              case DBCol::eColType_text:
                                  opStream << "\n\t\tself." << col->getColumnName() << "= [DatabaseAdapter getNullableString: aStmt atIndex: anIndex++];";
                                  break;
                              case DBCol::eColType_date:
                                  opStream << "\n\t\tself." << col->getColumnName() << "= [DatabaseAdapter getNullableDate: aStmt atIndex: anIndex++];";
                                  break;
                              case DBCol::eColType_datetime:
                                  opStream << "\n\t\tself." << col->getColumnName() << "= [DatabaseAdapter getNullableDateTime: aStmt atIndex: anIndex++];";
                                  break;
                              case DBCol::eColType_foreignkey:
                                  opStream << "\n\t\tself." << col->getColumnName() << " = sqlite3_column_int( aStmt, anIndex++);" ;
                                  break;
                              case DBCol::eColType_identity:
                                  opStream << "\n\t\tself.databaseId" << " = sqlite3_column_int( aStmt, anIndex++);" ;
                          }
                      });
    
    opStream <<    "\n\t}"\
    "\n\treturn self;"\
    "\n}";
}

// ------------------------------------------------------
void ModuleFileGenerator::generateInsertSQL( const std::unique_ptr<DBObject>& o)
{
    bool firstCol = true;
    // getInsertSQL
    opStream << "\n\n// ----------------------------------------"\
    "\n-(NSString *)getInsertSQL"\
    "\n{";
    // Handle the fields which can be NULL.
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          std::string name = col->getColumnName();
                          if( col->getColumnType() == DBCol::eColType_text)
                          {
                              opStream << "\n\tNSString* a_" << name << " = [[NSString alloc] initWithFormat: (" << name << "? @\"'%@'\" : @\"NULL\")," << name << "]; // Value like '<value>' or NULL";
                          }
                          else if ( col->getColumnType() == DBCol::eColType_datetime )
                          {
                              opStream << "\n\tNSString* a_" << name << " = [[NSString alloc] initWithFormat: (" << name << "? @\"'%@'\": @\"NULL\"), [[DatabaseAdapter getInstance ]formatDateTimeForDB:dob]];";
                          }
                          else if ( col->getColumnType() == DBCol::eColType_date )
                          {
                              opStream << "\n\tNSString* a_" << name << " = [[NSString alloc] initWithFormat: (" << name << "? @\"'%@'\": @\"NULL\"), [[DatabaseAdapter getInstance ]formatDateForDB:dob]];";
                          }
                      });
   opStream << "\n\tNSString* insertStr = [NSString stringWithFormat:@\"INSERT INTO " << o->getName() << "(";
    
    firstCol = true;
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              return;
                          
                          opStream << (firstCol ? "" :",") << col->getColumnName();
                          firstCol = false;
                      });
    opStream << ") VALUES (";
    firstCol = true;
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              return;
                          opStream << (firstCol ? "" :",") << FieldTypeToLocalType(col->getColumnType());
                          firstCol = false;
                      });
    opStream << ")\",";
    firstCol = true;
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              return;
                          opStream << (firstCol ? "\n\t\t" :",\n\t\t");
                          if( col->getColumnType()== DBCol::eColType_text)
                              opStream << "a_" << col->getColumnName();
                          else if( col->getColumnType()== DBCol::eColType_datetime)
                              opStream << "a_" << col->getColumnName();
                          else if( col->getColumnType()== DBCol::eColType_date)
                              opStream << "a_" << col->getColumnName();
                          else
                              opStream << col->getColumnName();
                          firstCol = false;
                      });
    opStream << "];\n\treturn insertStr;\n}";
}




// ------------------------------------------------------
void ModuleFileGenerator::generateUpdateSQL( const std::unique_ptr<DBObject>& o)
{
    bool firstCol = true;
    // getUpdateSQL
    opStream << "\n\n// ----------------------------------------"\
    "\n-(NSString*)getUpdateSQL"\
    "\n{";
    // Handle the fields which can be NULL.
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          std::string name = col->getColumnName();
                          if( col->getColumnType() == DBCol::eColType_text)
                          {
                              opStream << "\n\tNSString* a_" << name << " = [[NSString alloc] initWithFormat: (" << name << "? @\"'%@'\" : @\"NULL\")," << name << "]; // Value like '<value>' or NULL";
                          }
                          else if ( col->getColumnType() == DBCol::eColType_datetime )
                          {
                              opStream << "\n\tNSString* a_" << name << " = [[NSString alloc] initWithFormat: (" << name << "? @\"'%@'\": @\"NULL\"), [[DatabaseAdapter getInstance ]formatDateTimeForDB:dob]];";
                          }
                          else if ( col->getColumnType() == DBCol::eColType_date )
                          {
                              opStream << "\n\tNSString* a_" << name << " = [[NSString alloc] initWithFormat: (" << name << "? @\"'%@'\": @\"NULL\"), [[DatabaseAdapter getInstance ]formatDateForDB:dob]];";
                          }
                      });
    opStream << "\n\tNSString *updateSQL = [[NSString alloc] initWithFormat: @\"UPDATE " << o->getName() << " SET ";
    firstCol = true;
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              return;   // We dont update the identity column
                          if( col->getColumnType() == DBCol::eColType_text || col->getColumnType() == DBCol::eColType_date || col->getColumnType() == DBCol::eColType_datetime)
                              opStream <<  (firstCol? "":",") << col->getColumnName() << "=%@";
                          else
                              opStream <<  (firstCol? "":",") << col->getColumnName() << "=" << FieldTypeToLocalType( col->getColumnType());
                          
                          firstCol = false;
                      });
    opStream << " WHERE id=%ld\",";
    firstCol = true;
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              return;
                          if( !firstCol)
                              opStream <<",";
                          if( col->getColumnType() == DBCol::eColType_datetime || col->getColumnType() == DBCol::eColType_date || col->getColumnType() == DBCol::eColType_text)
                              opStream << "a_" << col->getColumnName();
                          else
                              opStream << col->getColumnName();
                          firstCol = false;
                      });
    opStream << ",databaseId];"\
    "\n\treturn updateSQL;"\
    "\n}\n\n";
}



// ------------------------------------------------------
void ModuleFileGenerator::generateForObject( const std::unique_ptr<DBObject>& o)
{
    opStream.str("");   // clear old stuff
    opStream << "\n// " << o->getName() << ".m" << m_CopyrightString;
    opStream << "\n#import <Foundation/Foundation.h>";
    opStream << "\n#include \"" << o->getName() << ".h\""\
        "\n#include <sqlite3.h>"\
        "\n#include \"DatabaseAdapter.h\""\
        "\n"\
        "\n//******************************************";
    opStream << "\n//      implementation"\
        "\n//******************************************"\
        "\n@implementation " << o->getName();
    // Generate @synthesize for each field
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              opStream<< "\n@synthesize databaseId;";
                          else
                              opStream << "\n@synthesize " << col->getColumnName() <<";";
                      });

    generateCreateTableStuff(o);
    getSelectByIdSQL(o);
    getDeleteSQL(o);    // deleteById and deletaAll
    
    // selectAllSQL
    opStream <<
        "\n"\
        "\n// ----------------------------------------"\
        "\n+(const char *)getSelectAllSQL"\
        "\n{"\
        "\n\treturn \"SELECT * from ";
    opStream << o->getName() << "\";"\
    "\n}";

    generateConstructors( o );
    generateInsertSQL( o );
    generateUpdateSQL( o );
    
    opStream << "-(NSString*)description"\
        "\n{"\
        "\n\tNSString* str = @\"\"; // TODO:"\
        "\n\treturn str;"\
        "\n}";
    
    opStream << "\n@end\n";
}



// ------------------------------------------------------
void  OBJCGenerator::doGenerate()
{
    std::cout << "\nGenerating for Objective - C...";
    std::string generateContent;
    
    std::cout << "\n\tGenerating DatabaseAdapter.h...";
    generateContent = generateDatabaseAdapterHeaderFile();
    if( this->m_Debug )
    {
        std::cout << generateContent;
    }
    else
    {
        std::ofstream f(this->m_Dictonary.getOutputPathForFile( eTargetTypes::eTargetType_objc,"DatabaseAdapter", ".h"));
        f << generateContent;
    }
    
    std::cout << "\n\tGenerating DatabaseAdapter.m...";
    generateContent = generateDatabaseAdapterModule();
    if( this->m_Debug )
    {
        std::cout << generateContent;
    }
    else
    {
        std::ofstream f(this->m_Dictonary.getOutputPathForFile(eTargetTypes::eTargetType_objc, "DatabaseAdapter", ".m"));
        f << generateContent;
    }

    HeaderFileGenerator hfg(this->m_Dictonary.getCopyrightString());
    ModuleFileGenerator mfg(this->m_Dictonary.getCopyrightString());
    
    this->m_Dictonary.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                                  {
                                      std::cout << "\n\tGenerating " << o->getName() << ".h...";
                                      hfg.generateForObject(o);
                                      if(  this->m_Debug ){
                                          std::cout << hfg.toStr();
                                      }
                                      else
                                      {
                                          std::ofstream f( this->m_Dictonary.getOutputPathForFile( eTargetTypes::eTargetType_objc,o->getName().c_str(), ".h"));
                                          f << hfg.toStr();
                                      }
                                      std::cout << "\n\tGenerating " << o->getName() << ".m...";
                                      mfg.generateForObject(o);
                                      
                                      if( this->m_Debug){
                                          std::cout << mfg.toStr();
                                      }
                                      else
                                      {
                                          std::ofstream f( this->m_Dictonary.getOutputPathForFile( eTargetTypes::eTargetType_objc, o->getName().c_str(), ".m"));
                                          f << mfg.toStr();
                                      }
                                  });
}

