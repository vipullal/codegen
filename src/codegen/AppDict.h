/*
 File: AppDict.h
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */
#ifndef AppDict_h
#define AppDict_h

#include <memory>
#include <string>
#include <vector>
#include <functional>
#include "TargetTypes.h"
#include "DBObject.h"


class AppDict
{
public:
    AppDict();
    void  addObject( std::unique_ptr<DBObject>& o )
    {
        m_Objects.push_back(std::move(o) );
    }
    
    // Call a member function of DBObject for each object
    void forEachObject( void (DBObject::*pmf)() )
    {
        std::for_each(m_Objects.begin(),m_Objects.end(), std::mem_fn( pmf ));
    }
    
    //void forEachObject( void (*func)( const std::unique_ptr<DBObject>&) ) const;
    void forEachObject( std::function< void (const std::unique_ptr<DBObject> &) >& func ) const;
    template<typename Functor>  void doEachObject( Functor f) const
    {
        for( auto & o: m_Objects)
            f(o);
    }

    std::string getOutputPath( eTargetTypes t) const  { return m_DestinationPaths[ getOrdinal(t) ];}
    std::string getOutputPathForFile(eTargetTypes t, const char *fileName, const char *extn) const;
    void setOutputPath( eTargetTypes t,const char *path );
    
    std::string getNamespace() const { return m_Namespace;}
    void setNamespace( const char *ns );
    
    std::string getProjectName() const { return m_ProjectName;}
    void setProjectName( const char *ns ){ m_ProjectName = std::string(ns);}
    
    std::string getCopyrightString() const ;
    
private:
    //std::string m_DestinationPath;
    std::vector<std::string>    m_DestinationPaths;
    
    std::string m_Namespace;
    std::vector< std::unique_ptr<DBObject> > m_Objects;
    mutable std::string  m_CopyrightString;
    std::string m_ProjectName;
};

#endif /* AppDict_h */
