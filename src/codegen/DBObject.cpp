/*
 File: DBObject.c
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#include "DBObject.h"
#include "tinyxml.h"
#include <stdexcept>
#include <strstream>
#include <algorithm>
#include <sstream>

// ----------------------------------
void DBObject::forEachField( std::function< void (const std::unique_ptr<DBCol> &)>& func )
{
    std::for_each( this->m_Columns.begin(), this->m_Columns.end(), func );
}

// ----------------------------------
void DBObject::generateSQL()
{
    generateCreateTableSQL();
}


// ----------------------------------
void DBObject::generateCreateTableSQL()
{
    std::string s = std::string("\"CREATE TABLE IF NOT EXIST ");
    s+=this->m_Name;
    s+="(\"";
    this->m_CreateTableSQL.push_back( s );
    size_t i=0;
    size_t max = this->m_Columns.size();
    auto colDefn = [&]( const std::unique_ptr<DBCol>& col)
    {
        if( col->getColumnType() == DBCol::eColType_identity)
            s = "\t\"id INTEGER PRIMARY KEY AUTOINCREMENT NUT NULL"; // special case..
        else
        {
            s = "\t\"";
            s += col->getColumnName();
            s += " ";
            s += col->getSQLType();
        }
        if( ++i < max )
            s+= ",";
        s+="\"";
        this->m_CreateTableSQL.push_back(s);
    };
    std::for_each( this->m_Columns.begin(), this->m_Columns.end(), colDefn );
    this->m_CreateTableSQL.push_back( std::string("\t\")\";"));
}


// ----------------------------------
std::string DBObject::getCreateTableSQL( int nIndent,char seperator )
{
    std::ostringstream os;
    std::string prefix(nIndent,'\t');
    size_t i = 0;
    size_t max = m_CreateTableSQL.size();
    for( auto s : m_CreateTableSQL)
    {
        os << (i==0? " ": prefix);
        os << s << (++i < max ? seperator : ' ') << "\n";
    }
    return os.str();
}



// ----------------------------------
std::unique_ptr<DBObject> DBObject::fromXML( const TiXmlNode* node)
{
    std::unique_ptr<DBObject> aObject(new DBObject());

    // Check that the current tag is a dbobject tag
    if( strcmp(node->Value(),"dbobject") ){
        throw std::invalid_argument("Expected dbobject tag");
    }
    const TiXmlElement* element = node->ToElement();
    if( !element )
        throw std::invalid_argument("Expected element");
    const char *objName = element->Attribute("name");
    if( !objName ){
        throw std::domain_error("Expected name in tag");
    }
    aObject->m_Name = std::string( objName,0, strlen(objName) );

    // Parse and validate...
    for( const TiXmlNode* a = node->FirstChild(); a ; a=a->NextSibling())
    {
        if( a->Type() == TiXmlNode::TINYXML_COMMENT)
            continue;
        std::unique_ptr<DBCol> aColumn = DBCol::fromXML( a );
        aObject->m_Columns.push_back( std::move(aColumn) );
    }
    return aObject;
};
