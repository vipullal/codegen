/*
 File: AndroidGenerator.cpp
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#include "AndroidGenerator.h"
#include <iostream>
#include <fstream>
#include <functional>
#include <sstream>


namespace
{
    const char *DBTypeToDataType( DBCol::eColumnTypes inType)
    {
        switch(inType)
        {
            case DBCol::eColType_int:
                return "int";
            case DBCol::eColType_number:
                return "double";
            case DBCol::eColType_bool:
                return "boolean";
            case DBCol::eColType_text:
                return "String";
            case DBCol::eColType_date:
                return "Date";     // todo: yyyymmdd
            case DBCol::eColType_datetime:
                return "Date";
            case DBCol::eColType_foreignkey:
            case DBCol::eColType_identity:
                return "long";
            default:
                return "--Invalid type--";
        }
    };


class DatabaseAdapterGenerator
{
    
    std::ostringstream  m_Stream;
    
    void generateDatabaseHelper();
    void generateUtilityMethods();
    void generateAddFunctionForObject(const std::unique_ptr<DBObject>& o);
    void generateDeleteAllFunctionForObject(const std::unique_ptr<DBObject>& o);
    void generateUpdateFunctionForObject(const std::unique_ptr<DBObject> &o);
    void generateGetAllFunctionForObject(const std::unique_ptr<DBObject> &o);
    void generateGetByIdFunctionForObject(const std::unique_ptr<DBObject> &o);
    void generateDeleteByIdFunctionForObject(const std::unique_ptr<DBObject> &o);
    
    void generateCRUIDForClass(const std::unique_ptr<DBObject>& o);
    const AppDict& m_Dict;
    
    public:
        DatabaseAdapterGenerator( const AppDict& dict ):m_Dict(dict){}
        void generate();
        std::string str()   {   return m_Stream.str();}
};



// ---------------------------------------------------
void DatabaseAdapterGenerator::generateDatabaseHelper()
{
    m_Stream << "\n\n\n\t/**"\
    "\n\t* This class helps open, create, and upgrade the database file."\
    "\n\t*/"\
    "\n\tprivate static class MyDatabaseHelper extends SQLiteOpenHelper"\
    "\n\t{"\
    "\n"\
    "\n\t\tMyDatabaseHelper(Context context){"\
    "\n\t\t\tsuper(context, DATABASE_NAME, null, DATABASE_VERSION);"\
    "\n\t\t}"\
    "\n"\
    "\n\t\t@Override"\
    "\n\t\tpublic void onCreate(SQLiteDatabase db) {"
    "\n\t\t\ttry{";
    
    this->m_Dict.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                                   {
                                       m_Stream << "\n\t\t\t\tdb.execSQL( " << o->getName() << ".getCreateTableSQL());";
                                   });
    
    m_Stream << "\n\t\t\t}"\
        "\n\t\t\tcatch( Exception ex){"
        "\n\t\t\t\tLog.e(TAG,\"Exception while creating the database\" + ex);"\
        "\n\t\t\t}"\
    "\n\t\t}"\
    "\n"\
    "\n\t\t@Override"\
    "\n\t\tpublic void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {"\
    "\n\t\t\tLog.w(TAG, \"Upgrading database from version \" + oldVersion + \" to \"+ newVersion + \", which will destroy all old data\");"\
    "\n\t\t\ttry{";
    this->m_Dict.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                              {
                                  m_Stream << "\n\t\t\t\tdb.execSQL( \"DROP TABLE IF EXISTS " << o->getName() << "\" );";
                              });

    m_Stream << "\n\t\t\t\tonCreate(db);"
        "\n\t\t\t}"\
        "\n\t\t\tcatch(Exception ex){"\
        "\n\t\t\t\tLog.e(TAG,\"Exception while updating the databases \" + ex);"\
        "\n\t\t\t}"\
        "\n\t\t}"\
        "\n\t}// class MyDatabaseHelper\n\n";
}

// ----------------------------------------------
void DatabaseAdapterGenerator::generateUtilityMethods()
{
    m_Stream <<"\n\n\t//----------------------------------------"\
        "\n\tDate getDateFromDateString(String inDateStr){"\
        "\n\t\tif( inDateStr == null || inDateStr.length() == 0)"\
        "\n\t\treturn null;"\
        "\n\t\ttry"\
        "\n\t\t{"\
        "\n\t\t\treturn sDateFormat.parse( inDateStr );"\
        "\n\t\t}"\
        "\n\t\tcatch (ParseException e)"\
        "\n\t\t{"\
        "\n\t\t\tLog.e(TAG,\"Exception while parsing date \" + e);"\
        "\n\t\t}"\
        "\n\t\treturn null;"\
        "\n\t}"\
        "\n\n\t//----------------------------------------"\
        "\n\tString getDateStringFromDate(Date d){"\
        "\n\t\tif( d!= null )"\
        "\n\t\t\treturn sDateFormat.format( d );"\
        "\n\t\treturn null;"\
        "\n\t}"
        "\n\n\t//----------------------------------------"\
        "\n\tDate getDateTimeFromDateTimeString(String inDateStr){"\
        "\n\t\tif( inDateStr == null || inDateStr.length() == 0)"\
        "\n\t\t\treturn null;"\
        "\n\t\ttry"\
        "\n\t\t{"\
        "\n\t\t\treturn sDateFormat_Full.parse( inDateStr );"\
        "\n\t\t}"\
        "\n\t\tcatch (ParseException e)"\
        "\n\t\t{"\
        "\n\t\t\tLog.e(TAG,\"Exception while parsing date \" + e);"\
        "\n\t\t}"\
        "\n\t\treturn null;"\
        "\n\t}"
        "\n\n\t//----------------------------------------"\
        "\n\tString getDateTimeStringFromDate(Date d){"\
        "\n\t\tif( d!= null )"\
        "\n\t\t\treturn sDateFormat_Full.format(d);"\
        "\n\t\treturn \"\";"\
        "\t}"\
        ""\
        "\n\n\t//----------------------------------------"\
        "\n\tboolean getBooleanFromInt(int v){"\
        "\n\t\treturn v == 0? false: true;"\
        "\n\t}"\
        "\n\n\t//----------------------------------------"\
        "\n\tint booleanToInt(boolean tf){"\
        "\n\t\treturn tf? 1:0;"\
        "\n\t}";
}

// ----------------------------------------------
void DatabaseAdapterGenerator::generateAddFunctionForObject(const std::unique_ptr<DBObject>& o)
{
    m_Stream << "\n\n\t//---------------------------------"
    << "\n\tboolean add" << o->getName() << "(" << o->getName() << " o){"\
    << "\n\t\tif( mLogging)"\
    << "\n\t\t\tLog.d(TAG, \"Add " << o->getName() << " called...\");"\
    << "\n\t\tSQLiteDatabase db = m_OpenHelper.getWritableDatabase();"\
    << "\n\t\tContentValues cv = o.toContentValues();"\
    << "\n\t\tlong id = db.insert( \"s" << o->getName() << "\", null, cv);"\
    << "\n\t\to.setDatabaseId( id );"\
    << "\n\t\treturn false;"\
    << "\n\t}";
}



// ----------------------------------------------
void DatabaseAdapterGenerator::generateGetAllFunctionForObject(const std::unique_ptr<DBObject> &o) {
    std::string oName = o->getName();
    m_Stream << "\n\n\t//---------------------------------"\
         "\n\t" << oName << "[] getAll" << oName<< "(){"\
         "\n\t\tif( mLogging)"\
         "\n\t\t\tLog.d(TAG, \"GetAll " << oName << " called...\");"\
        "\n\t\tArrayList<"<< oName << "> al = new ArrayList<" << oName << ">();"\
        "\n\t\tCursor c = null;"\
        "\n"\
        "\n\t\ttry"\
        "\n\t\t{"\
        "\n\t\t\tSQLiteDatabase db = m_OpenHelper.getReadableDatabase();"\
        "\n"\
        "\n\t\t\tc = db.query( \"" << oName << "\", null, null /*criterion*/, null, null, null, null);"\
        "\n"\
        "\n\t\t\tif( c.moveToFirst())"\
        "\n\t\t\t{"\
        "\n\t\t\t\tdo"\
        "\n\t\t\t\t{"\
        "\n"\
        "\n\t\t\t\t\t" << oName << " x = " << oName << ".fromCursor(c);"\
        "\n\t\t\t\t\tal.add(x);"\
        "\n\t\t\t\t}"\
        "\n\t\t\t\twhile( c.moveToNext());"\
        "\n\t\t\t}"\
        "\n\t\t}"\
        "\n\t\tcatch(Exception ex)"\
        "\n\t\t{"\
        "\n\t\t\tLog.e(TAG,\"Exception \" + ex);"\
        "\n\t\t}"\
        "\n\t\tfinally"\
        "\n\t\t{"\
        "\n\t\t\tif( c!= null )"\
        "\n\t\t\t\tc.close();"\
        "\n\t\t}"\
        "\n\t\treturn (" << oName << "[])(al.toArray( new " << oName << "[ al.size()]));"\
        "\n"\
        "\n\t}";
}

// ----------------------------------------------
void  DatabaseAdapterGenerator::generateUpdateFunctionForObject(const std::unique_ptr<DBObject> &o)
{
    m_Stream << "\n\n\t//---------------------------------"
    << "\n\tboolean update" << o->getName() << "(" << o->getName() << " o){"\
    << "\n\t\tif( mLogging)"\
    << "\n\t\t\tLog.d(TAG, \"Update " << o->getName() << " called...\");"\
    << "\n\t\tSQLiteDatabase db = m_OpenHelper.getWritableDatabase();"
    << "\n\t\treturn false;"\
    << "\n\t}";
}


// ----------------------------------------------
void  DatabaseAdapterGenerator::generateGetByIdFunctionForObject(const std::unique_ptr<DBObject> &o)
{
    m_Stream << "\n\n\t//---------------------------------"
    << "\n\t" << o->getName() << " get" << o->getName() << "WithId(long inId){"\
    << "\n\t\tif( mLogging)"\
    << "\n\t\t\tLog.d(TAG, \"Get " << o->getName() << "WithId called...\");"\
    << "\n\t\t" << o->getName() << " ret = null;"
    << "\n\t\tSQLiteDatabase db = m_OpenHelper.getReadableDatabase();"
    << "\n\t\tCursor c = db.query(" << o->getName() <<".getSelectByIdSQL(inId),null,null,null,null,null,null);"\
    << "\n\t\tif( c.moveToFirst()){"\
    << "\n\t\t\t ret = "<< o->getName() << ".fromCursor(c);"\
    << "\n\t\t}"\
    << "\n\t\treturn ret;"
    << "\n\t}";
}

// ----------------------------------------------
void  DatabaseAdapterGenerator::generateDeleteByIdFunctionForObject(const std::unique_ptr<DBObject> &o)
{
    m_Stream << "\n\n\t//---------------------------------"
    << "\n\tboolean delete" << o->getName() << "WithId(long inId){"\
    << "\n\t\tif( mLogging)"\
    << "\n\t\t\tLog.d(TAG, \"Delete " << o->getName() << "WithId called...\");"\
    << "\n\t\tSQLiteDatabase db = m_OpenHelper.getWritableDatabase();"
    << "\n\t\treturn db.delete( " << o->getName() << ".getDeleteByIdSQL(inId), null,null) > 0;"
    << "\n\t}";
}

// ----------------------------------------------
void DatabaseAdapterGenerator::generateDeleteAllFunctionForObject(const std::unique_ptr<DBObject>& o)
{
    m_Stream << "\n\n\t//---------------------------------"
    << "\n\tboolean deleteAll" << o->getName() << "(){"\
    << "\n\t\tif( mLogging)"\
    << "\n\t\t\tLog.d(TAG, \"Delete all " << o->getName() << " called...\");"\
    << "\n\t\tSQLiteDatabase db = m_OpenHelper.getWritableDatabase();"
    << "\n\t\treturn db.delete( " << o->getName() << ".getDeleteAllSQL(), null,null) > 0;"\
    << "\n\t}";
}


// ----------------------------------------------
void DatabaseAdapterGenerator::generateCRUIDForClass(const std::unique_ptr<DBObject>& o)
{
    m_Stream << "\n\n\t// CRUID for " << o->getName();
    
    // Add
    generateAddFunctionForObject(o);
    
    // Update
    generateUpdateFunctionForObject(o);
    
    // getById
    generateGetByIdFunctionForObject(o);
    
    // DeleteById
    generateDeleteByIdFunctionForObject(o);
    
    // DeleteAll
    generateDeleteAllFunctionForObject(o);
    
    // getAll
    generateGetAllFunctionForObject(o);
    
}

// ----------------------------------------------
void DatabaseAdapterGenerator::generate()
{
    m_Stream << "package " << this->m_Dict.getNamespace()  << ";";
    m_Stream << "\n\n\nimport java.util.ArrayList;"\
    "\nimport android.content.ContentValues;"\
    "\nimport android.content.Context;"\
    "\nimport android.database.Cursor;"\
    "\nimport java.util.Date;"\
    "\nimport android.database.sqlite.SQLiteDatabase;"\
    "\nimport android.database.sqlite.SQLiteException;"\
    "\nimport android.database.sqlite.SQLiteOpenHelper;"\
    "\nimport android.util.Log;"\
    "\nimport java.text.ParseException;"\
    "\nimport java.text.SimpleDateFormat;"\
    "\nimport java.util.Date;"\
    "\n\n\npublic class DatabaseAdapter"\
    "\n{"\
    "\n\tprivate static final String TAG = \"DatabaseAdapter\";"\
    "\n"\
    "\n\tprivate static final String DATABASE_NAME = \"" << this->m_Dict.getProjectName() << ".db\";"\
    "\n\tprivate static final int DATABASE_VERSION = 1;"\
    "\n\tprivate static final boolean mLogging = true;"\
    "\n\tstatic SimpleDateFormat sDateFormat = new SimpleDateFormat(\"yyyyMMdd\");"
    "\n\tstatic SimpleDateFormat sDateFormat_Full = new SimpleDateFormat(\"yyyyMMddHHmm\");"
    "\n";
    generateDatabaseHelper();
    m_Stream << "\n\tprivate MyDatabaseHelper m_OpenHelper;"\
    "\n\tstatic DatabaseAdapter s_Instance = new DatabaseAdapter();"\
    "\n"\
    "\n"\
    "\n\tstatic DatabaseAdapter getInstance(){"\
    "\n\t\treturn s_Instance;"\
    "\n\t}"\
    "\n"\
    "\n\t/*"\
    "\n\t* Constructor"\
    "\n\t*/"\
    "\n\tprivate DatabaseAdapter(){"\
    "\n\t\t// Nothing in the auto-generated code.."\
    "\n\t}"\
    "\n"\
    "\n\tvoid openForBusiness( Context ctx ) throws SQLiteException {"\
    "\n\t\tif( mLogging )"\
    "\n\t\t\tLog.d(TAG, \"DatabaseAdapter.openForBusiness called...\");"\
    "\n\t\tif( m_OpenHelper != null ) {"\
    "\n\t\t\tLog.e(TAG, \"Multiple calls to open !!!!\");"\
    "\n\t\t\treturn;"\
    "\n\t\t}"\
    "\n"\
    "\n\t\ttry{"\
    "\n\t\t\tm_OpenHelper = new MyDatabaseHelper( ctx );"\
    "\n\t\t}"\
    "\n\t\tcatch( SQLiteException ex){"\
    "\n\t\t\tLog.e(TAG, \"Exception while creating openHelper \" + ex);"\
    "\n\t\t\tthrow ex;"\
    "\n\t\t}"\
    "\n\t}\n";
    
    // Utility methods...
    generateUtilityMethods();
    
    
    // CRUID methods for each class...
    this->m_Dict.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                              {
                                  generateCRUIDForClass(o);
                              });
    
    m_Stream << "\n}\n";        // end of class
}
    
} // local namespace

// -------------------------------------
void AndroidGenerator::generateDatabaseAdapter()
{
    DatabaseAdapterGenerator generator( this->m_Dictonary );
    generator.generate();
    
    if( m_Debug )
    {
        std::cout << generator.str();
    }
    else{
        std::ofstream f( this->m_Dictonary.getOutputPathForFile( eTargetTypes::eTargetType_android, "DatabaseAdapter", ".java"));
        f << generator.str();
    }
}



// ----------------------------------------
class ClassFileGenerator
{
    std::ostringstream opStream;
    std::string m_CopyrightString;
    std::string m_PackageName;
    
    
    void generateCreateTableSQL( const std::unique_ptr<DBObject> & o );
    void generateToContentValues( const std::unique_ptr<DBObject> & o  );
    void generateFromCursor( const std::unique_ptr<DBObject> & o );
    void generateDeleteByIdSQL( const std::unique_ptr<DBObject> & o );
    void generateDeleteAllSQL( const std::unique_ptr<DBObject> & o );
    void generateSelectByIdSQL( const std::unique_ptr<DBObject> & o );

    void generateVariablesAndGettersAndSetters(const std::unique_ptr<DBObject> & o);
public:
    ClassFileGenerator( std::string inPackageName,std::string inCopyrightString )
    :m_CopyrightString(inCopyrightString),m_PackageName(inPackageName)
    {
    }
    void generateJavaCodeForObject(const std::unique_ptr<DBObject> & o );
    
    std::string toStr()
    {
        return opStream.str();
    }
};


// ----------------------------------------
void ClassFileGenerator::generateCreateTableSQL( const std::unique_ptr<DBObject> & o )
{
    // Create table SQL
    opStream << "\n\n\t// ------------------------------------------"\
        << "\n\tpublic static String getCreateTableSQL(){"
        << "\n\t\treturn " << o->getCreateTableSQL(2,'+') << "\n\t}\n";
}


// ----------------------------------------
void ClassFileGenerator::generateToContentValues( const std::unique_ptr<DBObject> & o  )
{
    // toContentValues
    opStream << "\n\n\t// ------------------------------------------"\
        << "\n\tpublic ContentValues toContentValues(){"\
        << "\n\t\tContentValues cv = new ContentValues();"\
        << "\n\t\ttry{";
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          switch(col->getColumnType())
                          {
                              case DBCol::eColType_int:
                                  opStream << "\n\t\t\tcv.put(\"" << col->getColumnName() << "\", this.m" << col->getColumnProperName() << ");" ;
                                  break;
                              case DBCol::eColType_number:
                                  opStream << "\n\t\t\tcv.put(\"" << col->getColumnName() << "\", this.m" << col->getColumnProperName() << ");" ;
                                  break;
                              case DBCol::eColType_bool:
                                  opStream << "\n\t\t\tcv.put(\"" << col->getColumnName() << "\", DatabaseAdapter.getInstance().booleanToInt( this.m" << col->getColumnProperName() << "));" ;
                                  break;
                              case DBCol::eColType_text:
                                  opStream << "\n\t\t\tif( this.m"<< col->getColumnProperName()<< " != null )";
                                  opStream << "\n\t\t\t\tcv.put(\"" << col->getColumnName() << "\", this.m" << col->getColumnProperName() << " );" ;
                                  break;
                              case DBCol::eColType_date:
                                  opStream << "\n\t\t\tif( this.m"<< col->getColumnProperName()<< " != null )";
                                  opStream << "\n\t\t\t\tcv.put(\"" << col->getColumnName() << "\", DatabaseAdapter.getInstance().getDateStringFromDate( this.m" << col->getColumnProperName() << "));" ;
                                  break;
                                  
                              case DBCol::eColType_datetime:
                                  opStream << "\n\t\t\tif( this.m"<< col->getColumnProperName()<< " != null )";
                                  opStream << "\n\t\t\t\tcv.put(\"" << col->getColumnName() << "\", DatabaseAdapter.getInstance().getDateTimeStringFromDate( this.m" << col->getColumnProperName() << "));" ;
                                  break;
                              case DBCol::eColType_foreignkey:
                                  opStream << "\n\t\t\tcv.put(\"" << col->getColumnName() << "\", this.m" << col->getColumnProperName() << ");" ;
                                  break;
                              case DBCol::eColType_identity:
                                  return;   // We never have to save the IDENTITY column to disk...
                              default:
                                  opStream <<  "-- undefined --;";
                          }
                      });
        opStream<< "\n\t\t}"\
            "\n\t\tcatch( Exception ex)"\
            "\n\t\t{"\
            "\n\t\t\t\tLog.e(TAG, \"Exception while saving toContentValues \" + ex);"\
            "\n\t\t}"\
            "\n\t\treturn cv;"\
            "\n\t}";
}


// ----------------------------------------
void ClassFileGenerator::generateFromCursor( const std::unique_ptr<DBObject> & o )
{
    // fromCursor
    opStream << "\n\n\t// ------------------------------------------"\
        << "\n\tpublic static " << o->getName() << " fromCursor(Cursor c ){"\
        << "\n\t\t" << o->getName() << " o = new " << o->getName() <<"();"\
        << "\n\t try"\
        << "\n\t\t{"
        << "\n\t\t\tint aIndex = 0;";
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          opStream << "\n\t\t\to.m" << (col->getColumnType()==DBCol::eColType_identity? "DatabaseId" : col->getColumnProperName()) << "=" ;
                          switch(col->getColumnType())
                          {
                              case DBCol::eColType_int:
                                  opStream <<  "c.getInt( aIndex++);";
                                  break;
                              case DBCol::eColType_number:
                                  opStream <<  "c.getDouble( aIndex++);";
                                  break;
                              case DBCol::eColType_bool:
                                  opStream <<  "DatabaseAdapter.getInstance().getBooleanFromInt( c.getInt( aIndex++));";
                                  break;
                              case DBCol::eColType_text:
                                  opStream <<  " ( c.isNull(aIndex) ? null: c.getString( aIndex) );"
                                      << "\n\t\t\t++aIndex;";
                                  break;
                              case DBCol::eColType_date:
                                  opStream <<  "DatabaseAdapter.getInstance().getDateFromDateString( c.getString( aIndex++));";
                                  break;
                              case DBCol::eColType_datetime:
                                  opStream <<  "DatabaseAdapter.getInstance().getDateTimeFromDateTimeString( c.getString( aIndex++));";
                                  break;
                              case DBCol::eColType_identity:
                                  opStream <<  "c.getLong( aIndex++);";
                                  break;
                              case DBCol::eColType_foreignkey:
                                  opStream <<  "c.getLong( aIndex++);";
                                  break;
                              default:
                                  opStream <<  "-- undefined --;";  // create a syntax error!
                          }
                      });
    opStream << "\n\t\t}"\
        << "\n\t\tcatch(Exception ex)"\
        << "\n\t\t{"\
        << "\n\t\t\tLog.e(TAG,\"Exception while retrieving results \" + ex);"\
        << "\n\t\t}"\
        << "\n\t\treturn o;"\
        << "\n\t}\n";

}



// ----------------------------------------
void ClassFileGenerator::generateDeleteByIdSQL( const std::unique_ptr<DBObject> & o )
{
    opStream << "\n\n\tpublic static String getDeleteByIdSQL( long inId){"\
    "\n\t\treturn \"DELETE * FROM " << o->getName() << " WHERE id=\"+" << "inId;"\
    "\n\t}";

}

// ----------------------------------------
void ClassFileGenerator::generateDeleteAllSQL( const std::unique_ptr<DBObject> & o )
{
    opStream << "\n\n\tpublic static String getDeleteAllSQL(){"\
        "\n\t\treturn \"DELETE FROM " << o->getName() <<"\";"\
        "\n\t}";

}

// ----------------------------------------
void ClassFileGenerator::generateSelectByIdSQL( const std::unique_ptr<DBObject> & o )
{
    opStream << "\n\n\tpublic static String getSelectByIdSQL( long inId ){"\
    "\n\t\treturn \"SELECT * FROM " << o->getName() << " WHERE id=\"+ inId;"\
    "\n\t}";
}


// ----------------------------------------
void ClassFileGenerator::generateVariablesAndGettersAndSetters(const std::unique_ptr<DBObject> & o)
{
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          if( col->getColumnType() == DBCol::eColType_identity)
                              opStream << "\n\tprivate " << DBTypeToDataType(col->getColumnType()) << "\t\tmDatabaseId;";
                          else
                              opStream << "\n\tprivate " << DBTypeToDataType(col->getColumnType()) << "\t\tm" << col->getColumnProperName() <<";";
                      });
    
    opStream << "\n\n\n\t//Getters and Setters ...\n";
    o->doForEachField([&](const std::unique_ptr<DBCol> & col)
                      {
                          std::string fldName = col->getColumnProperName();
                          if( col->getColumnType() == DBCol::eColType_identity)
                              fldName = "DatabaseId";
                          opStream << "\n\t" << DBTypeToDataType(col->getColumnType()) << " get" << fldName << "(){";
                          opStream << "\n\t\t return m" << fldName << ";\n\t}\n";
                          opStream << "\n\tvoid " << " set" << fldName << "(" << DBTypeToDataType(col->getColumnType()) <<" " << col->getColumnName() << "){";
                          opStream << "\n\t\t this.m" << fldName << " = "<<  col->getColumnName() << ";\n\t}\n";

                      });
}

// ----------------------------------------
void ClassFileGenerator::generateJavaCodeForObject(const std::unique_ptr<DBObject> & o )
{
    opStream.str("");   // clear old stuffand reuse stream
    opStream << m_CopyrightString;
    opStream << "\npackage " << m_PackageName << ";\n"\
        << "\n\nimport android.database.Cursor;"\
        << "\nimport android.content.ContentValues;"\
        << "\nimport java.util.Date;"\
    << "\nimport android.util.Log;";
    
    
    opStream << "\n\n\npublic class " << o->getName() << " {"\
        "\n"\
        "\n\tprivate static final String TAG=\"" << o->getName() <<"\"; // for logging.\n";
    generateVariablesAndGettersAndSetters( o );
    generateCreateTableSQL(o);
    generateToContentValues( o );
    generateFromCursor( o );
    generateDeleteByIdSQL( o );
    generateDeleteAllSQL( o );
    generateSelectByIdSQL( o );
    opStream << "\n}\n";

    
}

// -------------------------------------
void AndroidGenerator::generteClassFiles()
{
    ClassFileGenerator generator(this->m_Dictonary.getNamespace(), this->m_Dictonary.getCopyrightString());
    this->m_Dictonary.doEachObject( [&]( const std::unique_ptr<DBObject> & o )
                                   {
                                       std::cout <<"\n\tGenerating class file for " << o->getName() <<"...";
                                       generator.generateJavaCodeForObject(o);
                                       
                                       if( this->m_Debug ){
                                           std::cout << generator.toStr();
                                       }
                                       else
                                       {
                                           std::ofstream f( this->m_Dictonary.getOutputPathForFile( eTargetTypes::eTargetType_android,o->getName().c_str(), ".java"));
                                           f << generator.toStr();
                                       }
                                   });
    
}



// -------------------------------------
void  AndroidGenerator::doGenerate()
{
    std::cout << "Generating for Android...";
    std::cout <<"\n\tGenerating DatabaseAdapter for Android...";
    generateDatabaseAdapter();
    generteClassFiles();
    std::cout <<"\nCongratulation! Android code genertion done...\n";

}
