/*
 IPlatformGenerator.cpp
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#include "IPlatformGenerator.h"
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>


// -----------------------------------
void IPlatformGenerator::ensureOutputPathExists() const 
{
    std::string aPath = this->getOutputPath();
    struct stat info;
    
    if( aPath.length() == 0)
    {
        std::cout << "\nFiles will be created in " << system("pwd");
        return;     // No path specified.
    }
    
    if( stat( aPath.c_str(), &info ) != 0 )
    {
        std::cout <<"\n" << aPath << " -> path not found. Creating...";
        std::string cmd = "mkdir -p ";
        cmd += aPath;
        std::cout << "\n" << cmd << " returns " <<system( cmd.c_str() );
    }
    else if( info.st_mode & S_IFDIR )  // S_ISDIR() doesn't exist on my windows
    {
        std::cout << "Files will be written to " << aPath;
    }
    else
        std::cout << aPath << "is no directory\n";
    
}
