/*
 File: CodeGen.h
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#ifndef CodeGen_h
#define CodeGen_h
#include <string>
#include <vector>
#include <memory>
#include "AppDict.h"
#include "IPlatformGenerator.h"
#include "TargetTypes.h"


class CodeGen
{
    
public:
    
    CodeGen(): m_Target(eTargetTypes::eTargetType_all), m_Debug(false)
    {
    }
    
    
    void generateCode(int argc, const char **argv);
private:
    void configureFromArgs( int args, const char **argv);
    void showUsageAndExit(const char *msg );
    void parseAndroidTag( const TiXmlNode* node );
    void parseObjcTag( const TiXmlNode* node );
    void prepass();
    bool buildDictonary();
    
    std::string m_FileName;                 // XML File name
    eTargetTypes m_Target;                  // types to generate
    bool m_Debug;
    
    AppDict     m_Dictonary;
    std::vector< IPlatformGenerator*>    m_Platforms;
};

#endif /* CodeGen_h */
