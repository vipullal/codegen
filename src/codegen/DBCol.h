/*
 DBCol.h
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#ifndef DBCol_h
#define DBCol_h
#include <string>
#include <memory>



class TiXmlNode;

class DBCol
{
public:
    enum eColumnTypes
    {
        eColType_int = 1,
        eColType_number = 2,
        eColType_bool = 4,
        eColType_text = 8,
        eColType_date = 16,
        eColType_datetime = 32,
        eColType_identity = 64,
        eColType_foreignkey = 128
    };
    
    eColumnTypes colTypeFromTxt( const char *inTxt );
    
    std::string getColumnName()    {return m_Name ;}
    std::string getColumnProperName()    {return m_ProperName ;}    // First letter forced to UpperCase
    eColumnTypes getColumnType()   { return m_ColType;}
    int getColumnLength()           { return m_Length;}
    int getColumnPrecession()           { return m_Precession;}

    std::string getSQLType();
    std::string getCreateTableDefn()      {return m_CreateTableDefn;}
    
    static std::unique_ptr<DBCol> fromXML( const TiXmlNode* node);
    
private:
    DBCol()
    {
        m_Length = m_Precession = 0;
    }
    std::string m_Name;
    std::string m_ProperName;
    std::string m_CreateTableDefn;
    eColumnTypes  m_ColType;
    int m_Length;       // for numeric types
    int m_Precession;   // for numeric types.
    
};

#endif /* DBCol_h */
