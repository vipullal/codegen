/*
 DBObject.h
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#ifndef DBObject_h
#define DBObject_h
#include <string>
#include <vector>
#include <memory>
#include <functional>
#include "DBCol.h"

class TiXmlNode;

class DBObject
{
public:
    void generateSQL();
    const std::string getName()  {return m_Name;}       // This is the TableName as well!
    std::string getCreateTableSQL(int nIndent,char seperator);
    size_t getColumnCount()         {return m_Columns.size();}
    void forEachField( std::function< void (const std::unique_ptr<DBCol> &) >& func );
    template<typename Functor>  void doForEachField( Functor f) const
    {
        for( auto & o: m_Columns)
            f(o);
    }
    static std::unique_ptr<DBObject> fromXML( const TiXmlNode* node);
private:
    DBObject(){}    // default
    void generateCreateTableSQL();
    void generateInsertTableSQL();
    
    std::vector< std::unique_ptr<DBCol> >  m_Columns;
    std::string m_Name;             // Table name
    std::vector<std::string> m_CreateTableSQL;   // CREATE TABLE IF NOT EXISTS...
    std::string m_InsertIntoSQL;
    std::string m_GetByID;          // SELECT * FROM ...
    
    
};

#endif /* DBObject_h */
