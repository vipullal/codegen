/*
 File: DBCol.cpp
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */
#include "DBCol.h"
#include "tinyxml.h"
#include <stdexcept>


// -------------------------------------------
DBCol::eColumnTypes DBCol::colTypeFromTxt( const char *inText )
{
    if( strcmp(inText,"int") == 0 )
        return eColType_int;
    else if( strcmp(inText,"identity") == 0)
        return eColType_identity;
    else if( strcmp(inText,"numeric") == 0 )
        return eColType_number;
    else if( strcmp(inText,"bool") == 0 )
        return eColType_bool;
    else if( strcmp(inText,"text") == 0 || strcmp( inText,"string") == 0)
        return eColType_text;
    else if( strcmp(inText,"date") == 0 )
        return eColType_date;
    else if( strcmp(inText,"datetime") == 0 )
        return eColType_datetime;
    else if( strcmp(inText,"foreignkey") == 0 )
        return eColType_foreignkey;
    else
        throw std::invalid_argument(std::string("Invalid column type: ") + inText );     // TODO: Put in the column type passed
}

// -------------------------------------------
std::string DBCol::getSQLType()
{
    // TODO: https://sqlite.org/datatype3.html
    switch( this->m_ColType )
    {
        case eColType_int:
        case eColType_identity:
        case eColType_foreignkey:
            return std::string("INTEGER");
        case eColType_number:
            return std::string("REAL");
        case eColType_bool:
            return std::string("INTEGER");
        case eColType_text:
            return std::string("TEXT");
        case eColType_date:
            return std::string("TEXT");
        case eColType_datetime:
            return std::string("TEXT");
        default:
            throw std::invalid_argument("Invalid column type ");
    }
}


// -------------------------------------------
std::unique_ptr<DBCol> DBCol::fromXML( const TiXmlNode* node)
{
    std::unique_ptr<DBCol> aCol(new DBCol() );
    // get the element from the node
    const TiXmlElement* element = node->ToElement();
    if( !element )
        throw std::invalid_argument("Expected element");
    const char* colName = element->Attribute("name");
    const char* colType = element->Attribute("type");
    if( strcmp(colType,"identity") == 0 ){
        colName="id";
    }
    aCol->m_Name = std::string( colName,0, strlen(colName));
    std::string aProperName =  aCol->m_Name;
    aProperName[0] = toupper(aProperName[0]);
    aCol->m_ProperName = aProperName;
    aCol->m_ColType = aCol->colTypeFromTxt( colType);
    if( aCol->m_ColType == DBCol::eColType_number)
    {
        // TODO: Get the length and prec from the XML. For now, assume 22,4
        aCol->m_Length = 22;
        aCol->m_Precession = 4;
    }
    if( aCol->m_ColType == DBCol::eColType_identity)
        aCol->m_CreateTableDefn = "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL";
    else{
        aCol->m_CreateTableDefn = aCol->m_Name;
        aCol->m_CreateTableDefn += " ";
        aCol->m_CreateTableDefn += aCol->getSQLType();
    }
    return aCol;
}
