/*
 OBJCGenerator.h
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */


#ifndef OBJCGenerator_h
#define OBJCGenerator_h

#include "IPlatformGenerator.h"
#include <memory>
#include <sstream>

class OBJCGenerator: public IPlatformGenerator
{
    std::string  generateDatabaseAdapterHeaderFile();
    std::string  generateDatabaseAdapterModule();

    void generateClassFile( const std::unique_ptr<DBObject>& );
    void generateHeaderFile( const std::unique_ptr<DBObject>&  );
    
public:
    OBJCGenerator(const AppDict & inDict, bool inDebug)
    :IPlatformGenerator(inDict, inDebug)
    {
    }
    void  doGenerate() override;
    
    std::string getOutputPath() const override
    {
        return m_Dictonary.getOutputPath(eTargetTypes::eTargetType_objc );
    }

};

#endif /* OBJCGenerator_h */
