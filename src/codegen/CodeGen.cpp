/*
 File: CodeGen.cpp
 Project: codegen
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */
#include "CodeGen.h"
#include <iostream>
#include "tinyxml.h"
#include "AppDict.h"
#include "DBObject.h"

#include "AndroidGenerator.h"
#include "OBJCGenerator.h"
#include "SWIFTGenerator.h"
#include "TargetTypes.h"



using std::placeholders::_1;


// ----------------------------------
void CodeGen::showUsageAndExit(const char* msg )
{
    if( msg )
        std::cout << "\n" << msg << " aborting...";
    std::cout << "\nUsage is: codegen <xmlFile> <platforms> <-d> "\
        "\nWhere:"\
    "\n\t<xmlfile>*: is the file containing the database schema"\
    "\n\t<platforms>: Can be Android, ios or all (case insensitive, defults to ALL)"\
    "\n\t-d: (Debug) if specified, all output will be echo'ed to the command line. Default is to generate code."
    "\nFor details on the format of the xmlfile, please check the sample xml file."
    "\n-h: show this help."\
    "\n";
    exit(1);
}

// ----------------------------------
void CodeGen::configureFromArgs( int argc, const char **argv)
{
    
    if( argc < 2)
        showUsageAndExit("Incorrect number of args");
    if( std::string("-h").compare(argv[1]) == 0 )
        showUsageAndExit( nullptr);
    
    // get the path to the .xml file.
    m_FileName = std::string( argv[1]);
    
    // Get the target platforms, default to ALL
    if( argc > 2){
        std::string platforms(argv[2]);
        std::transform(platforms.begin(), platforms.end(),platforms.begin(), ::toupper);
        if( platforms.compare(std::string("ANDROID")) == 0 )
            m_Target = eTargetTypes::eTargetType_android;
        else if( platforms.compare(std::string("IOS")) == 0 )
            m_Target = eTargetTypes::eTargetType_objc;
        else if( platforms.compare(std::string("SWIFT")) == 0 )
            m_Target = eTargetTypes::eTargetType_swift;
        else if( platforms.compare(std::string("ALL")) == 0 )
            m_Target = eTargetTypes::eTargetType_all;
        else
            showUsageAndExit("Invalid platrform type specified");
    }
    else{
        m_Target = eTargetTypes::eTargetType_all;
    }
    
    // Get the remaining args...
    for(int i = 3;i < argc; i++){
        std::string arg( argv[i]);
        if( arg.compare("-d") == 0){
            std::cout <<"\nDebug only switch found. I will not write output files, only output to console...";
            this->m_Debug = true;
        }
    }
}

// ----------------------------------
void CodeGen::generateCode(int argc, const char **argv)
{
    configureFromArgs(argc, argv );
    std::cout << "\nGenerating code from file [" << this->m_FileName << "]\n";
    switch( this->m_Target)
    {
        case eTargetTypes::eTargetType_objc:
            m_Platforms.push_back( new OBJCGenerator( m_Dictonary, m_Debug ) );
            break;
        case eTargetTypes::eTargetType_swift:
            m_Platforms.push_back( new SWIFTGenerator( m_Dictonary, m_Debug ) );
            break;
        case eTargetTypes::eTargetType_android:
            m_Platforms.push_back( new AndroidGenerator( m_Dictonary, m_Debug ) );
            break;
        case eTargetTypes::eTargetType_all:
            m_Platforms.push_back( new OBJCGenerator( m_Dictonary, m_Debug ) );
            m_Platforms.push_back( new SWIFTGenerator( m_Dictonary, m_Debug ) );
            m_Platforms.push_back( new AndroidGenerator( m_Dictonary, m_Debug ) );
    }
    buildDictonary();
    prepass();
    std::for_each(m_Platforms.begin(), m_Platforms.end(), std::mem_fun(&IPlatformGenerator::doGenerate));    //(&IPlatformGenerator::doGenerate, _1, this->m_Dictonary));
}

// ----------------------------------
void CodeGen::prepass()
{
}



// ----------------------------------
void CodeGen::parseAndroidTag( const TiXmlNode* node )
{
    const TiXmlNode* nsElement = node->FirstChild("namespace");
    const TiXmlNode* pathElement = node->FirstChild("genpath");
    for( const TiXmlNode* child = nsElement->FirstChild(); child; child = child->NextSibling() )
    {
        if( child->Type() == TiXmlNode::TINYXML_COMMENT)
            continue;
        if( child->Type() != TiXmlNode::TINYXML_TEXT )
            showUsageAndExit("Fatal error. Unexpected element in Android tag...");
        const char *v =child->Value();
        this->m_Dictonary.setNamespace(v);
        break;
    }
    for( const TiXmlNode* child = pathElement->FirstChild(); child; child = child->NextSibling() )
    {
        if( child->Type() == TiXmlNode::TINYXML_COMMENT)
            continue;
        if( child->Type() != TiXmlNode::TINYXML_TEXT )
            showUsageAndExit("Fatal error. Unexpected element in Android tag...");
        const char *p =child->Value();
         this->m_Dictonary.setOutputPath(eTargetTypes::eTargetType_android, p);
        break;
    }
}


// ----------------------------------
void CodeGen::parseObjcTag( const TiXmlNode* node)
{
    const TiXmlNode* pathElement = node->FirstChild("genpath");
    for( const TiXmlNode* child = pathElement->FirstChild(); child; child = child->NextSibling() )
    {
        if( child->Type() == TiXmlNode::TINYXML_COMMENT)
            continue;
        if( child->Type() != TiXmlNode::TINYXML_TEXT )
            showUsageAndExit("Fatal error. Unexpected element in Objc tag...");
        const char *p =child->Value();
        this->m_Dictonary.setOutputPath(eTargetTypes::eTargetType_objc, p);
        break;
    }

}

// ----------------------------------
bool CodeGen::buildDictonary()
{
    TiXmlDocument doc(this->m_FileName.c_str());
    doc.LoadFile();
    const TiXmlElement* rootElement = doc.RootElement();
    if(!rootElement){
        std::cout << "could not open file or file is not proper XML\n";
        return -1;
    }
    if( strcmp(rootElement->Value(),"project") ){
        std::cout << "Unknown root element " << rootElement->Value() << "] must be <\"project...\"> aborting...\n";
        return -1;
    }
    const char* projName = rootElement->Attribute("name");
    if( !projName ){
        std::cout <<"\n--------No name attribute on project tag !";
        projName = "Dummy";
    }
    this->m_Dictonary.setProjectName(projName);
    
    for( const TiXmlNode* child = rootElement->FirstChild(); child; child = child->NextSibling() )
    {
        if( child->Type() == TiXmlNode::TINYXML_COMMENT)
            continue;
        std::string x = child->Value();
        if( x.compare("android") == 0 ){
            parseAndroidTag( child );
        }
        else if( x.compare("objc") == 0 ){
            parseObjcTag( child );
        }
        else if( x.compare("dbobject") == 0 ){
            std::unique_ptr<DBObject> obj( DBObject::fromXML( child ) );
            this->m_Dictonary.addObject(obj);
        }
        else{
            showUsageAndExit("Fatal error. Expected android, ios od DBObject tag...");
        }
    }
    this->m_Dictonary.forEachObject( &DBObject::generateSQL );
    return true;
}

