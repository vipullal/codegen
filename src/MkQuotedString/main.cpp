/*
 File: main.cpp
 Project: MkQuotedString
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any
 damages arising from the use of this software.
 
 Permission is granted to anyone to use this software for any
 purpose, including commercial applications, and to alter it and
 redistribute it freely, subject to the following restrictions:
 
 1. The origin of this software must not be misrepresented; you must
 not claim that you wrote the original software. If you use this
 software in a product, an acknowledgment in the product documentation
 would be appreciated but is not required.
 
 2. Altered source versions must be plainly marked as such, and
 must not be misrepresented as being the original software.
 
 3. This notice may not be removed or altered from any source
 distribution.
 
 -- Vipul Lal, www.FarEastSoftware.com
 */

#include <iostream>
#include <fstream>


void usage()
{
    std::cout << "This program will read a file line by line and "\
    "make a C++ cmpatible string. Used by codegen\n"
    "usage is MkQuotedName <file>\n";
}


void convertFile( const char *name){
    std::cout << "\nConverting file..." << name << "\n\n";
    std::ifstream f( name);
    char ch;
    if(!f){
        std::cout << "\nCould not open file? ";
        return;
    }
    bool isNewLine = true;
    do{
        f.get(ch);
        if(!f)  // EOF?
            break;
        if( isNewLine ){
            std::cout << "\"\\n";
            isNewLine = false;
        }
        if( ch == '\t'){
            std::cout << "\\t";
        }
        else if( ch == '\n'){
            std::cout << "\"\\\n";
            isNewLine = true;
        }
        else if( ch == '"' ){
            std::cout << "\\\"";
        }
        else
            std::cout << ch;
    }
    while( f);
    
}

int main(int argc, const char * argv[]) {
    if( argc < 2 ){
        usage();
        return 1;
    }
    convertFile( argv[1]);
    return 0;
}
